
# Working on the docker image
## Prerequisites 
To run the tests you have to install goss. Follow the instructions [here](https://medium.com/@aelsabbahy/tutorial-how-to-test-your-docker-image-in-half-a-second-bbd13e06a4a9).

## Building the container
To build the container run:

```console
docker build -t handbreak .
```

## Running the container
To run the container

```console
docker run --name docker-cron -it handbreak
```

this will create a container called docker-cron.

## Working with the container

```console
docker exec -it handbreak /bin/bash
```

## Testing the docker container

See more details about testing the container [here](https://medium.com/@aelsabbahy/tutorial-how-to-test-your-docker-image-in-half-a-second-bbd13e06a4a9). You can run the tests by running: 

```console
bash run.sh
```

See the inside of that file for the tests that are being run. GOSS_FILE should have been used instead of moving from directory to directory but it does not seem to work, so I am moving into directories to be able to test things.

## Cleaning up
To delete the container from the local machine run:

```console
docker container rm handbreak
```

## Running the container

### Using the detault "Fast 720p30" preset
```console
docker run --name handbreak --rm -it --mount type=bind,source="$(pwd)",target=/process registry.gitlab.com/mihai-p/handbreak:latest /root/handbrake.sh
```

### Using the "Fast 1080p30" preset
```console
docker run --name handbreak --rm -it -e PRESET_NAME="Fast 1080p30" --mount type=bind,source="$(pwd)",target=/process registry.gitlab.com/mihai-p/handbreak:latest /root/handbrake.sh
```


