FROM ubuntu:18.04

MAINTAINER Mihai Petrescu <mihai.petrescu@gmail.com>

RUN apt-get update

RUN apt-get install -y software-properties-common

# install handbreak
RUN add-apt-repository ppa:stebbins/handbrake-releases
RUN apt-get install -y handbrake-cli

# Add the processing file
COPY handbrake.sh /root/handbrake.sh
RUN chmod +x /root/handbrake.sh

# Add the testing file
COPY test.sh /root/test.sh
RUN chmod +x /root/test.sh