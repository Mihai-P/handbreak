#!/usr/bin/env bash
echo '**************************************'
echo '*           INITIAL FINDINGS          *'
echo '**************************************'

IFS='
' # split on newline only
set -o noglob # disable globbing

SOURCE="/process"
[[ -z "$PRESET_NAME" ]] && PRESET_NAME="Fast 720p30"
[[ -z "$DESIRED_RESOLUTION" ]] && DESIRED_RESOLUTION=$(echo $PRESET_NAME | grep -o -E '[0-9]+' | head -1 | sed -e 's/^0\+//')
[[ -z "$DESTINATION" ]] && DESTINATION=$SOURCE
[[ -z "$SEARCH_FOR" ]] && SEARCH_FOR="720p,1080p,1440p,2160p"
DESTINATION=$(echo $DESTINATION | sed 's:/*$::')

IFS=',' read -r -a searcharray <<< $SEARCH_FOR

SEARCH_REGEXP=$(IFS=$"|";echo "${searcharray[*]}")

echo "Destination: $DESTINATION"
echo "Desired Resolution: $DESIRED_RESOLUTION"
echo "Search Regexp: $SEARCH_REGEXP"

for FILE in `find ${SOURCE}/ -type f -regextype posix-extended -regex ".*($SEARCH_REGEXP).*\.(mkv|avi|mp4)" | grep -v "processed" | head -25 | xargs -0 -I filename echo filename`; do
        if [ -e "$FILE" ]; then
                echo '**************************************'
                echo "processing file ${FILE}"
                FILENAME=$(basename -- "${FILE}")
                BASEPATH=$(dirname "${FILE}")
                NEWNAME=$FILENAME
                
                for element in "${searcharray[@]}"
                do
                        NEWNAME=`echo "$NEWNAME" | sed -e "s/${element}/${DESIRED_RESOLUTION}p.processed/"`
                done

                NEWBASEPATH=`echo "$BASEPATH" | sed -e "s#${SOURCE}#${DESTINATION}#"`
                NEWFILE="${NEWBASEPATH}/${NEWNAME}"

                TMPNAME=`echo "$NEWNAME-TEMP"`
                TMPFILE="${BASEPATH}/${TMPNAME}"
                                
                echo "Converting ${FILE} to ${NEWFILE}"
                HandBrakeCLI -i "${FILE}" -o "${TMPFILE}" --preset="${PRESET_NAME}"  > /dev/null 2> /dev/null
                if [ $? -eq 0 ]; then
                        echo "conversion successfull, output to ${TMPFILE}"
                        if [ -z "$PRESERVE" ]
                        then
                                echo "removing old file"
                                rm ${FILE}
                        fi
                        NEWDIR=$(dirname "${NEWFILE}")
                        mkdir -p $NEWDIR
                        echo "moving temporary file form ${TMPFILE} to ${NEWFILE}"
                        mv "${TMPFILE}" "${NEWFILE}"
                        
                else
                    	echo "conversion failed, removing the tmp file if it exists and sending an email"
                        rm ${NEWFILE}
                        # mail -s “Enter the subject” user1@domain.com
                fi
        fi
done
