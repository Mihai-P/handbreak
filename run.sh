#!/usr/bin/env bash
docker build -t handbreak .
GOSS_SLEEP='30s' dgoss run -v $(pwd)/test:/process --env DESTINATION=/tmp/ --env PRESERVE=1 -it handbreak /root/test.sh
git checkout test/.
# cleanup
rm -rf "test/Big Buck Bunny-small-720p.processed.mp4"
rm -rf "test/Oregon-720p.processed.mp4"
rm -rf "test/bats-small-720p.processed.mp4"
rm -rf "test/sub space/test-720p.processed.mp4"

echo "*******************************"
echo "*           gossmove          *"
echo "*******************************"
cd gossmove
GOSS_SLEEP='30s' dgoss run -v $(pwd)/../test:/process --env DESTINATION=/tmp/ -it handbreak /root/test.sh
cd ../
git checkout test/.

# cleanup
rm -rf "test/Big Buck Bunny-small-720p.processed.mp4"
rm -rf "test/Oregon-720p.processed.mp4"
rm -rf "test/bats-small-720p.processed.mp4"
rm -rf "test/sub space/test-720p.processed.mp4"

echo "*******************************"
echo "*           gosssame          *"
echo "*******************************"
cd gosssame
GOSS_SLEEP='30s' dgoss run -v $(pwd)/../test:/process -it handbreak /root/test.sh 2>&1
cd ../
git checkout test/.

# cleanup
rm -rf "test/Big Buck Bunny-small-720p.processed.mp4"
rm -rf "test/Oregon-720p.processed.mp4"
rm -rf "test/bats-small-720p.processed.mp4"
rm -rf "test/sub space/test-720p.processed.mp4"

echo "*******************************"
echo "*           goss2160          *"
echo "*******************************"
cd goss2160
GOSS_SLEEP='30s' dgoss run -v $(pwd)/../test:/process --env DESTINATION=/tmp/ --env SEARCH_FOR="1080p,2160p" -it handbreak /root/test.sh 2>&1
cd ../
git checkout test/.

# cleanup
rm -rf "test/Big Buck Bunny-small-720p.processed.mp4"
rm -rf "test/Oregon-720p.processed.mp4"
rm -rf "test/bats-small-720p.processed.mp4"
rm -rf "test/sub space/test-720p.processed.mp4"


echo "*******************************"
echo "*           goss1080          *"
echo "*******************************"
cd goss1080
GOSS_SLEEP='30s' dgoss run -v $(pwd)/../test:/process --env DESTINATION=/tmp/ --env SEARCH_FOR="1080p" -it handbreak /root/test.sh 2>&1
cd ../
git checkout test/.

# cleanup
rm -rf "test/Big Buck Bunny-small-720p.processed.mp4"
rm -rf "test/Oregon-720p.processed.mp4"
rm -rf "test/bats-small-720p.processed.mp4"
rm -rf "test/sub space/test-720p.processed.mp4"
